## INDUSTRIAL REVOLUTION
* **INDUSTRY 1.0** -Mechanization, Steam power, Weaving loom
* **INDUSTRY 2.0** -Mass production, Assembly line, Electrical energy
* **INDUSTRY 3.0** -Automation, Computers and Electronics
* **INDUSTRY 4.0** -Cyber physical systems, Internet of Things, Network

### Industry 3.0: Data generally stored in database and represented in excels.
SCADA & ERP  
Sensors installed at various points in the factory send data to PLC's which collect all the data and send it to SCADA and ERP systems for storing the data. Usually this data is stored in Excels and CSV's and rearely get plotted as real-time graphs or charts.

### Industry 3.0 communication protocols :
All the protocols are optimized for sending data to a central server inside the factory
* Modbus
* PROFINET
* CANopen
* EtherCAT

![iot](https://researchleap.com/wp-content/uploads/2020/01/3-3.jpg)

### Industry 4.0 communication protocols
All these protocols are optimized for sending data to cloud for data analysis
* MQTT
* AMQP
* OPCUA
* CoAP
* Websockets
* HTTP
* RESTful API

### Problems with Industry 4.0 upgrades
* Cost 
* Downtime 
* Reliability 
 ### Solution:
 Get data from Industry 3.0 devices/meters/sensors without changes to the original device and then send the data to the cloud using Industry 4.0 devices: 
 * By converting Industry 3.0 protocols to industry 4.0 protocols

 ![1](https://www.scnsoft.com/blog-pictures/internet-of-things/iot-architecture.png) 

 ### Data analysis
 * Iot TSDB Tools
   * Prometheus
   * InfluxDB
 * IoT Dashboards
   * Grafana
   * Thingsboard
 * IoT Platforms
   * AWS IoT
   * Google IoT 
   * Azure IoT
   * Thingsboard
 * Get Alerts
   * Zaiper
   * Twilio

 ### Example for Inside the IoT cloud
 ![cloud](https://www.emnify.com/hubfs/Cloud%20Connect%20elements-20.png)       
